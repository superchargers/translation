<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Translation;

/**
 * Api version :
 * 
 * Translation class - example of calling methods :
 * 
 * $translation = new Translation($userName, $password);
 * 
 * Example of calling translation method :
 * 
 * $translation->withToken($token) 
 *          ->withLanguageFrom($languageFrom)
 *          ->withLanguageTo($languageTo)
 *          ->withCallbackUrl($completedCallbackUrl)
 *          ->withReference($reference) // optional
 *          ->withJobDue($jobDue) //optional
 *          ->withPreTranslate() // optional 
 *          ->translation($filePath, null); If you want to send file for translation
 *          OR 
 *          ->translation(null, $text); If you want to send text to translation 
 * 
 * returns $taskId of yours translation task
 * 
 * Example of calling downloadTask method :
 * 
 * $translation->withToken()
 *          ->downloadTask($taskId, $fileName);
 * 
 * You can import token for every translation/downloadTask call with chainable method withToken($token), 
 * $translation->getToken() returns your access token which you can store in your DB and use next 24 hours until it expires.
 * 
 * parameters : 
 * $completedCallbackUrl - url which we will call when your translation task is done
 * $reference - yours task identifier, optional parameter
 * $languages_to - array of strings (language codes), example : SK, EN, DE
 * $language_from - string (language code), example : SK, EN, DE
 * $jobDue - date, optional parameter
 * $fileName - directory path with file name
 * 
 */

class Translation
{
    private $downloadUrl = 'https://translate.translata.sk/api/translated/';
    private $translateUrl = 'https://translate.translata.sk/api/translate';
    private $getTokenUrl = 'https://translate.translata.sk/oauth/token';
    private $createWebhookUrl = 'https://translate.translata.sk/api/webhook';
    private $deleteWebhookUrl = 'https://translate.translata.sk/api/webhook/'; 
    private $clientId = 2;
    private $clientSecret = '24ysQFcPQsk6XdkM4YoSjMMbm9vg4EMVk1U24050';
    private $userName;
    private $password;
    private $token;   
    private $apiVersion = 1;
    
//    parameters
    private $completedCallbackUrl;
    private $reference;
    private $languages_to;
    private $language_from;
    private $jobDue;
    private $preTranslate;
    
    /**
     * 
     * @param string $userName
     * @param string $password
     */

    public function __construct($userName, $password)
    {
        $this->userName = $userName;
        $this->password = $password;
    }
    
    public function getToken()
    {        
        $data = 'grant_type=password&username=' . $this->userName . '&password=' . $this->password . '&client_id=' . $this->clientId . '&client_secret=' . $this->clientSecret;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->getTokenUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($curl);

        curl_close($curl);
        
        $authData = json_decode($response, true);
        
        return $authData['access_token'];
    }
    
    /**
     * 
     * @param string $filePath
     * @param string $text
     * @return json
     */
    
    public function translate($filePath = null, $text = null)
    {
        $this->requireToken(); 
        
        ini_set('display_errors', 1);
        
        if($filePath)
        {
            if (defined('PHP_MAJOR_VERSION') && PHP_MAJOR_VERSION > 5.5) 
            {
                $file = new \CURLFile($filePath);
            }
            else
            {
                $file = '@' . realpath($filePath);
            }
        }
        else
        {
            $file = null;
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->translateUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept:application/json',
                'Authorization: Bearer '.$this->token,
                'Version:'. $this->apiVersion
            ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        if($file != null)
        {            
            if(filesize($filePath) == 0)
            {
                throw new \Exception('The selected file is empty!');
            }
            
            curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                'file' => $file, //, "txt/xlsx", 'toTranslate.xlsx'), // type of file extension, client original name
                'targetLan_codes' => $this->languages_to,
                'sourceLan_code' => $this->language_from,
                'job_due'  => $this->jobDue, 
                'callback_url' => $this->completedCallbackUrl,
                'reference' => $this->reference,
                'pre_translate' => $this->preTranslate
            ));
        }
        else if($text != null)
        {
            curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                'text' => $text, 
                'targetLan_codes' => $this->languages_to,
                'sourceLan_code' => $this->language_from,
                'job_due'  => $this->jobDue, 
                'callback_url' => $this->completedCallbackUrl,
                'reference' => $this->reference,
                'pre_translate' => $this->preTranslate
            ));
        }
        
        $response = curl_exec($curl);

        curl_close($curl);
        
        $data = json_decode($response, true);
        
        return $data;
    }
    
    /**
     * 
     * @param string $webhookName
     * @param string $webhookUrl
     * @param string $webhookType
     * @return json
     */
    
    public function createWebhook($webhookName, $webhookUrl, $webhookType = null)
    {
        $this->requireToken();
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->createWebhookUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept:application/json',
                'Authorization: Bearer '.$this->token,
                'Version:'. $this->apiVersion
            ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $body = [            
            'name' => $webhookName,
            'url' => $webhookUrl,
            'type' => $webhookType,
            ];
        
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));      
        
        $response = curl_exec($curl);

        curl_close($curl);
        
        $data = json_decode($response, true);
        
        return $data;
    }
    
    /**
     * 
     * @param string $webhookNameOrUrl
     * @return json
     */
    
    public function deleteWebhook($webhookNameOrUrl)
    {
        $this->requireToken();
        
        $deleteUrl = $this->deleteWebhookUrl . $webhookNameOrUrl;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $deleteUrl);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept:application/json',
                'Authorization: Bearer '.$this->token,
                'Version:'. $this->apiVersion
            ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        $data = json_decode($response, true);
          
        return $data;
    }
    
    /**
     * 
     * @param integer $taskIdOrReference
     * @param string $path
     * @return array
     */
    
    public function downloadTask($taskIdOrReference, $path = null)
    {
        $this->requireToken();
        
        $curl=curl_init();
        
        $downloadUrl = $this->downloadUrl . $taskIdOrReference;

        \Log::info($downloadUrl);
        
        //curl_setopt($curl, CURLOPT_FILE, $out);
        curl_setopt($curl, CURLOPT_URL, $downloadUrl);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Authorization: Bearer '.$this->token,
            'Version:'. $this->apiVersion
        ));   
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTREDIR, 3);
        //curl_setopt($curl, CURLOPT_POST, true);
        
        $response = curl_exec($curl);
        
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        
        $data = json_decode($body);

        file_put_contents($path . $data->filename, $data->file);
        
        return [
            'taskId' => $data->taskId,
            'filename' => $data->filename,
            'file' => $data->file
        ];
    }
    
    /**
    * 
     * @param string $languageFrom
     * @return $this
     */
    
    public function withLanaguageFrom($languageFrom)
    {
        $this->language_from = $languageFrom;
        
        return $this;
    }
    
    /**
     * 
     * @param string|array $languagesTo
     * @return $this
     */
    
    public function withLanguagesTo($languagesTo)
    {
        $this->languages_to = $languagesTo;
        
        return $this;
    }
    
    /**
     * 
     * @param string $completedCallbackUrl
     * @return $this
     */

    public function withCallbackUrl($completedCallbackUrl)
    {
        $this->completedCallbackUrl = $completedCallbackUrl;
        
        return $this;
    }
    
    /**
     * 
     * @param string $reference
     * @return $this
     */
    
    public function withReference($reference)
    {
        $this->reference = $reference;
        
        return $this;
    }

    /**
     * 
     * @param date $jobDue
     * @return $this
     */
    
    public function withJobDue($jobDue)
    {
        $this->jobDue = $jobDue;
        
        return $this;
    }
    
    /**
     * 
     * @return $this
     */
    
    public function withPreTranslate()
    {
        $this->preTranslate = 1;
        
        return $this;
    }
    
    /**
     * 
     * @param string $token
     * @return $this
     */

    public function withToken($token)
    {
        $this->token = $token;

        return $this;
    }
    
    private function requireToken()
    {
        if(!$this->token)
        {
            $this->token = $this->getToken();
        }
    }
}
